<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- CSS only -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <link rel="stylesheet" href="/bootstrap/dist/css/bootstrap.css">
    <link rel="stylesheet" href="../css/moncss.css">
    <title>UL-INFO</title>
</head>
<style> 
/* reset */
*{
    margin: 0;
    padding: 0;
}
.myFont
{
    font-family: "Droid Sans Mono", "DejaVu Sans Mono", "Monospace", monospace;
     text-align: center;
    font-size: 20px;
  }
.navbar
{padding: 1em,0;
background-color: #000;
height: 2em;
}
.navbar .navbar-right
{

    font-size: 0.8em;
    float: right;
    padding-top: 0.8em;
}
.navbar .navbar-right li
{
display: inline-block;
}
.navbar .navbar-right li a 
{
    text-decoration:none;
    color: white;

}
.navbar .navbar-right li a:hover
{
    color: #1ce;
}
footer
{
    position:absolute;
    bottom: 0px;
    width: 100%;

}
footer p 
{

}
</style>

<body class="myFont">
   <div class="container" >
    <header>
        <nav class="navbar">
            <ul class="navbar-right" >
                <li><a href="/" class="btn btn-info">ACUEIL</a></li>
                <li><a href="login">SE CONNECTER</a></li>
                <li><a href="About-us">A PROPOS</a></li>
            </ul>
        </nav>
    </header>
    <section>
           @yield('content')
    </section>
    <footer class="container-lg">
        <p>
            &copy;copyright CIC-UL&middot;{{date('Y')}}
        </p>
    </footer>
   </div>
</body>
</html>
