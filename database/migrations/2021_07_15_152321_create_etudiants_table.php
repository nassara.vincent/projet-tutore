<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEtudiantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('etudiants', function (Blueprint $table) {
            $table->integer('etudiant_id');
            $table->string('nom');
            $table->string('prenom');
            $table->string('loginId');
            $table->integer('id_paiement');
            $table->timestamps();
            $table->primary('etudiant_id');
            $table->foreign('loginId')->references('loginId')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('etudiants');
    }
}
