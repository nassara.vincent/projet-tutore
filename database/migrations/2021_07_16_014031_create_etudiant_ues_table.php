<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEtudiantUesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('etudiant_ues', function (Blueprint $table) {
            $table->integer('etudiant_id');
            $table->string('code_ue');
            $table->primary(['etudiant_id','code_ue']);
            $table->foreign('etudiant_id')->references('etudiant_id')->on('etudiants');
            $table->foreign('code_ue')->references('code_ue')->on('ues');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('etudiant_ues');
    }
}
