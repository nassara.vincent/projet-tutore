<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHomeManagersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('home_managers', function (Blueprint $table) {
            $table->integer('home_id');
            $table->string('loginId');
            $table->string('nomOrganisation');
            $table->timestamps();
            $table->primary('home_id');
            $table->foreign('loginId')->references('loginId')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('home_managers');
    }
}
