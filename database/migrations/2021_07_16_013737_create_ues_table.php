<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ues', function (Blueprint $table) {
            $table->string('code_ue');
            $table->string('intitule');
            $table->string('semestre');
            $table->string('code_depart');
            $table->integer('ens_id');
            $table->foreign('ens_id')->references('ens_id')->on('enseignants');
            $table->primary('code_ue');
            $table->foreign('code_depart')->references('code_depart')->on('departements');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ues');
    }
}
