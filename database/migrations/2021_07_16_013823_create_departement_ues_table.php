<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDepartementUesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('departement_ues', function (Blueprint $table) {
            $table->string('code_depart');
            $table->string('code_ue');
            $table->primary(['code_ue','code_depart']);
            $table->foreign('code_depart')->references('code_depart')->on('departements');
            $table->foreign('code_ue')->references('code_ue')->on('ues');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('departement_ues');
    }
}
