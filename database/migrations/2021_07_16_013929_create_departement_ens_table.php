<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDepartementEnsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('departement_ens', function (Blueprint $table) {
            $table->string('code_depart');
            $table->integer('ens_id');
            $table->primary(['code_depart','ens_id']);
            $table->foreign('code_depart')->references('code_depart')->on('departements');
            $table->foreign('ens_id')->references('ens_id')->on('enseignants');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('departement_ens');
    }
}
